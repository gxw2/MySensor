package com.gxw.mysensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.widget.TextView;
import com.gxw.mysensor.R;

public class MainActivity extends Activity {
	// private TextView textViewInfo = null;
	private TextView textView2 = null;
	private TextView textView3 = null;

	private TextView textView4 = null;
	private TextView textView5 = null;
	private TextView textView6 = null;

	private TextView textView7 = null;
	private TextView textView8 = null;
	private TextView textView9 = null;

	private TextView textView10 = null;
	private TextView textView11 = null;
	private TextView textView12 = null;

	private TextView textView13 = null;

	private SensorManager sensorManagerA = null;
	private Sensor sensorA = null;
	private float gravityA[] = new float[3];

	private SensorManager sensorManagerC = null;
	private Sensor sensorC = null;
	private float gravityC[] = new float[3];

	private SensorManager sensorManagerS = null;
	private Sensor sensorS = null;
	private float gravityS[] = new float[3];

	private SensorManager sensorManagerX = null;
	private Sensor sensorX = null;
	private float gravityX[] = new float[3];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// textViewInfo = (TextView) findViewById(R.id.TextView01);
		textView2 = (TextView) findViewById(R.id.TextView02);
		textView3 = (TextView) findViewById(R.id.TextView03);
		textView4 = (TextView) findViewById(R.id.TextView04);

		textView5 = (TextView) findViewById(R.id.TextView05);
		textView6 = (TextView) findViewById(R.id.TextView06);
		textView7 = (TextView) findViewById(R.id.TextView07);

		textView8 = (TextView) findViewById(R.id.TextView08);
		textView9 = (TextView) findViewById(R.id.TextView09);
		textView10 = (TextView) findViewById(R.id.TextView10);

		textView11 = (TextView) findViewById(R.id.TextView11);
		textView12 = (TextView) findViewById(R.id.TextView12);
		textView13 = (TextView) findViewById(R.id.TextView13);

		sensorManagerA = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensorA = sensorManagerA.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		sensorManagerC = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensorC = sensorManagerC.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		sensorManagerS = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensorS = sensorManagerS.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		sensorManagerX = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensorX = sensorManagerX.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

		// textViewInfo.setText("手机Accelerometer sensor详细信息：\n" +
		// "设备名称：  " + sensor.getName() + "\n" +
		// "设备供应商：  " + sensor.getVendor() + "\n" +
		// "设备功率：  " + sensor.getPower()+ "\n");
	}

	private SensorEventListener listenerA = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {

		}

		@Override
		public void onSensorChanged(SensorEvent e) {
			gravityA[0] = e.values[0];
			gravityA[1] = e.values[1];
			gravityA[2] = e.values[2];
			textView2.setText("X加速度：       " + gravityA[0]);
			textView3.setText("Y加速度：       " + gravityA[1]);
			textView4.setText("Z加速度：       " + gravityA[2]);
		}
	};

	private SensorEventListener listenerC = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {

		}

		@Override
		public void onSensorChanged(SensorEvent e) {
			gravityC[0] = e.values[0];
			gravityC[1] = e.values[1];
			gravityC[2] = e.values[2];
			textView5.setText("X磁力：       " + gravityC[0]);
			textView6.setText("Y磁力：       " + gravityC[1]);
			textView7.setText("Z磁力：       " + gravityC[2]);
		}
	};

	private SensorEventListener listenerS = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {

		}

		@Override
		public void onSensorChanged(SensorEvent e) {
			gravityS[0] = e.values[0];
			gravityS[1] = e.values[1];
			gravityS[2] = e.values[2];
			textView8.setText("X角速度：       " + gravityS[0]);
			textView9.setText("Y角速度：       " + gravityS[1]);
			textView10.setText("Z角速度：       " + gravityS[2]);
		}
	};

	private SensorEventListener listenerX = new SensorEventListener() {
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {

		}

		@Override
		public void onSensorChanged(SensorEvent e) {
			gravityX[0] = e.values[0];
			gravityX[1] = e.values[1];
			gravityX[2] = e.values[2];
			textView11.setText("X旋转矢量：       " + gravityX[0]);
			textView12.setText("Y旋转矢量：       " + gravityX[1]);
			textView13.setText("Z旋转矢量：       " + gravityX[2]);
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		sensorManagerA.registerListener(listenerA, sensorA,
				SensorManager.SENSOR_DELAY_NORMAL);
		sensorManagerC.registerListener(listenerC, sensorC,
				SensorManager.SENSOR_DELAY_NORMAL);
		sensorManagerS.registerListener(listenerS, sensorS,
				SensorManager.SENSOR_DELAY_NORMAL);
		sensorManagerX.registerListener(listenerX, sensorX,
				SensorManager.SENSOR_DELAY_NORMAL);

	}

	@Override
	protected void onStop() {
		super.onStop();
		sensorManagerA.unregisterListener(listenerA);
		sensorManagerC.unregisterListener(listenerC);
		sensorManagerS.unregisterListener(listenerS);
		sensorManagerX.unregisterListener(listenerX);
	}

}
